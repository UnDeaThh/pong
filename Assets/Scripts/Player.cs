﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidadY;

    public float maxY;
    private float posY;
    private float direction;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        direction = Input.GetAxis("Vertical");

        posY = transform.position.y + direction* velocidadY * Time.deltaTime;
        
        if (posY>maxY){
            posY = maxY;
        }
        if (posY< (maxY*-1)){
            posY = (maxY*-1);
        }
        
        transform.position = new Vector3(transform.position.x, posY, transform.position.z);    
        
    
        
    }
}
