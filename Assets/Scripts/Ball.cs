﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float velocidadX;
    public float velocidadY;
    private float posX;
    private float posY;
    public float maxX;
    public float maxY;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
     posX = transform.position.x + velocidadX * Time.deltaTime;
     posY = transform.position.y + velocidadY * Time.deltaTime;

     if (posY>maxY){
            posY = maxY;
            velocidadY*=-1;
        }
        if (posY< (maxY*-1)){
            posY = (maxY*-1);
            velocidadY*=-1;
        }
    if (posX>maxX){
            posX = maxX;
            velocidadX*=-1;
        }
        if (posX< (maxX*-1)){
            posX = (maxX*-1);
            velocidadX*=-1;
        }
     transform.position = new Vector3(posX, posY, transform.position.z);
        
        
    }
}
